import utils from '@/utils/index'
import Vue from 'vue'
import Vuex from 'vuex'
//  设置一个全局的store，通过this.$store访问
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    login: false,
    searchHistory: [], // storage(JHSH:求职者搜索历史,RSH:招聘者搜索历史)
    avatar: {}, //  上传头像
    config: null, //  所有参数
    userInfo: {},
    myresume: {}, //  我的简历信息，用于更新my页面的信息
    myschool: null, // 我的园所信息，用于更新my页面的信息
    switchover: false, //  身份切换的标识,为true时进入首页重新加载数据
    school: { name: '' }, //  编辑简历工作经验时园所查找时选择的学校信息
    unionid: '', // 首次绑定手机号时发送过去
    setAddress_temporary: '', //   临时变量，修改个人简历的个人信息的工作地址时用
    recruiter: null, // 招聘者身份信息
    weiUserInfo: {} //  微信用户信息
  },
  mutations: {
    setWeiUserInfo: (state, data) => {
      state.weiUserInfo = data
    },
    setAddress_temporary: (state, data) => {
      state.setAddress_temporary = data
    },
    setunionid: (state, val) => {
      state.unionid = val
    },
    setMyresume: (state, val) => {
      state.myresume = val
    },
    setRecruiter: (state, val) => {
      state.recruiter = val
    },
    setConfig: (state, config) => {
      state.config = config
    },
    setUserInfo: (state, userInfo) => {
      state.userInfo = userInfo
    },
    loginStatus: (state, val) => {
      state.login = val
    },
    changeSwitch: (state) => {
      state.switchover = false
    },
    selectSchool: (state, val) => {
      state.school = val
    },
    setMySchool: (state, school) => {
      state.myschool = school
    },
    setRecruit: (state, val) => {
      state.recruit = val
    },
    /**
     * 搜索历史记录的处理
     */
    getHistory: (state) => {
      let key
      state.userInfo && state.userInfo.identity === 2 ? key = 'RSH' : key = 'JHSH'
      state.searchHistory = wx.getStorageSync(key)
    },
    addHistory: (state, text) => {
      let key
      state.userInfo && state.userInfo.identity === 2 ? key = 'RSH' : key = 'JHSH'
      if (state.searchHistory.length === 0) {
        state.searchHistory = []
      }
      if (state.searchHistory.indexOf(text) >= 0) {
        state.searchHistory.splice(state.searchHistory.indexOf(text), 1)
      }
      state.searchHistory.unshift(text)
      wx.setStorageSync(key, state.searchHistory)
    },
    emptyHistory: (state) => {
      let key
      state.userInfo && state.userInfo.identity === 2 ? key = 'RSH' : key = 'JHSH'
      state.searchHistory = []
      wx.setStorageSync(key, state.searchHistory)
    },
    /**
     * 更换头像
     */
    changeAvatar: (state, src) => {
      state.avatar = src
    },
    changeIdentity: (state, status) => {
      state.userInfo.identity = status
      utils.toast('切换成功！')
      state.switchover = true
      setTimeout(() => {
        wx.switchTab({ url: '/pages/tab-bar/index' })
      }, 500)
    }
  },
  actions: {
    /**
     * 获取所有参数
     */
    getConfig: async(context) => {
      const localConfig = wx.getStorageSync('config') || null
      if (!localConfig || (Number(context.state.userInfo.configversion) !== Number(localConfig.configversion) && context.state.login)) {
        const config = await utils.ajax('/config')
        context.commit('setConfig', config.data)
        wx.setStorageSync('config', config.data)
      } else {
        if (!context.state.config) {
          context.commit('setConfig', localConfig)
        }
      }
    },
    /**
     * 获取用户信息
     *
     */
    getUserInfo: async(context) => {
      const userInfo = await utils.ajax('/user')
      context.commit('setUserInfo', userInfo.data)
      if (userInfo.data.user) {
        context.commit('loginStatus', true)
      }
    },
    changeIdentity: async(context, status) => {
      wx.showLoading({
        title: '切换中',
        mask: true
      })
      const result = await utils.ajax('/updataidentity?identity=' + status)
      context.commit('changeIdentity', result.data.identity)
      wx.hideLoading()
    }
  }
})
export default {
  store
}
