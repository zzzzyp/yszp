import Vuex from 'vuex'
const store = new Vuex.Store({
  state: {
    welfare: null,
    jobDesc: '',
    address: null,
    address_detailed: '',
    baseInfo: '',
    address_temporary: null
  },
  mutations: {
    addWelfare: (state, obj) => {
      state.welfare = obj;
    },
    addJobDesc: (state, text) => {
      state.jobDesc = text.synopsis;
    },
    address: (state, address) => {
      typeof (address) === 'object' ? state.address = address: '';
      typeof (address) === 'string' ? state.address_detailed = address: '';
    },
    setAddress_temporary: (state, text) => {
      state.address_temporary = text;
    },
    addBaseInfo: (state, info) => {
      state.baseInfo = info;
    }
  }
});
export { store };