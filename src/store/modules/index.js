import Vuex from 'vuex'
const store = new Vuex.Store({
  state: {
    condition: {
      expectJob: {},
      expectCity: {}
    },
    expectCity: {}
  },
  mutations: {
    addCondition: (state, val) => {
      state.condition = val;
      wx.setStorageSync('subscription', val);
    },
    setExpectCity: (state, val) => {
      state.expectCity = val;
      wx.setStorageSync('expectCity', val);
    },
    getCondition: (state) => {
      wx.getStorageSync('subscription').expectJob ? state.condition.expectJob = wx.getStorageSync('subscription').expectJob : state.condition.expectJob = { name: '不限', code: '' };
      wx.getStorageSync('subscription').expectCity ? state.condition.expectCity = wx.getStorageSync('subscription').expectCity : state.condition.expectCity = { name: '全国', code: 0 };
    }
  }
});
export { store };