import Vuex from 'vuex'
const store = new Vuex.Store({
  state: {
    schoolAvatar: '',
    principalAvatar: null,
    baseInfo: {},
    mySchool: false, // 填充值用
    setAddress_temporary: null
  },
  mutations: {
    uploadS: (state, src) => {
      state.schoolAvatar = src;
    },
    uploadP: (state, src) => {
      state.principalAvatar = src;
    },
    setMySchool: (state, data) => {
      state.mySchool = data;
    },
    setAddress_temporary: (state, data) => {
      state.setAddress_temporary = data;
    }
  }
});
export { store };