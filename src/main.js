import Vue from 'vue'
import App from './App'
import reset from '@/utils/reset'
import store from '@/store/global'
require('core-js/library/modules/_global.js').console = console;
Vue.config.productionTip = false
App.mpType = 'app'
//  临时解决相同页面跳转数据不会更新的问题,多页面居然共用一个对象,真你妈要命
reset.install(Vue);
Vue.prototype.$store = store.store;
const app = new Vue(App)
app.$mount()

export default {
  // 这个字段走 app.json
  config: {
    // 页面前带有 ^ 符号的，会被编译成首页，其他页面可以选填，我们会自动把 webpack entry 里面的入口页面加进去
    window: {
      backgroundTextStyle: 'dark',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '园所直聘',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      list: [{
        iconPath: 'static/img/ic_chance_nor.png',
        selectedIconPath: 'static/img/ic_chance_selected.png',
        pagePath: 'pages/tab-bar/index',
        text: '机会'
      }, {
        iconPath: 'static/img/icon-me_nor.png',
        selectedIconPath: 'static/img/icon-me_selected.png',
        pagePath: 'pages/tab-bar/my',
        text: '我的'
      }],
      backgroundColor: '#fff'
    }
  }
}