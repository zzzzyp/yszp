import qiniuUploader from '@/utils/qiniuUploader'
import store from '@/store/global'
/**
 *
 * @param {String} text - 提示文本
 */
function toast(text) {
  wx.showToast({
    title: text,
    icon: 'none',
    duration: 1500,
    mask: false
  })
}
/**
 *
 * @param {String} url - 请求url
 * @param {Object} data  -  请求体，data存在时，请求为POST否则GET
 */
function ajax(url, data, showLoading = false) {
  let token = wx.getStorageSync('token') ? 'Bearer' + wx.getStorageSync('token') : '',
    method
  typeof data === 'object' ? method = 'POST' : method = 'GET'
  typeof data === 'boolean' || showLoading ? wx.showLoading({ title: '加载中', mask: true }) : ''
  return new Promise(function(reslove, reject) {
    const requestTask = wx.request({
      //  本地：http://192.168.0.212 线上：http://47.106.192.174:88
      url: 'https://www.zyt360.com/api' + url,
      method: method,
      data: data,
      header: { 'content-type': 'application/x-www-form-urlencoded', 'Authorization': token, 'device': 'wx' },
      success: (res) => {
        /**
         * 异常状态码
         * 400001=>请刷新token=>token过期
         * 400002=>请带入token=>token缺失
         * 400003=>请从新登陆=>token失效
         * 400004=>该token查找不到用户=>用户不存在
         * 出现异常状态码清空token返回至my页面
         */
        console.log('请求地址：', url)
        console.log('返回值：', res.data)
        if (res.data.status === 400000 || res.data.status === 400001 || res.data.status === 400002 || res.data.status === 400003 || res.data.status === 400003 || res.data.status === 400004) {
          store.store.commit('loginStatus', false)
          toast(res.data.msg + '请重新登录')
          wx.removeStorageSync('token')
          const page = getCurrentPages()[getCurrentPages().length - 1].route
          page.indexOf('pages/tab-bar/index') >= 0 ? '' : wx.switchTab({ url: '/pages/tab-bar/my' })
          reslove(res)
          return
        }
        if (Number(res.data.status) <= 0) { // status小于等于0为服务端报错,直接抛出
          wx.showModal({
            title: '提示',
            content: res.data.msg,
            showCancel: false
          })
          throw res.data.msg
        }
        if (!res.data.status) {
          wx.showModal({
            title: '提示',
            content: res.data.message,
            showCancel: false
          })
          throw res.data.message
        }
        reslove(res.data)
      },
      fail: (err) => {
        reject(err)
      },
      complete: () => {
        typeof data === 'boolean' || showLoading ? wx.hideLoading() : ''
      }
    })
  })
}

/**
 * 验证方法
 * @param {Boolean or Object} condition - 为Object时根据type去进行验证，先这样写，后期想想怎么优化
 * @param {String} hint
 */
function validate(condition, hint) {
  if (typeof condition === 'object') {
    switch (condition.type) {
      case 'phone':
        if ((/(^1[3|4|5|7|8|9]\d{9}$)|(^09\d{8}$)/).test(String(condition.value))) {
          return true
        } else {
          toast('请输入正确的手机号')
          return false
        }
        break
      case 'name':
        if (condition.value.length > 0) { return true } else {
          toast('请填写姓名')
          return false
        }
        break
      case 'birthday':
        if (condition.value.length > 0) { return true } else {
          toast('请选择出生年月')
          return false
        }
        break
      case 'sex':
        if (condition.value.length > 0) { return true } else {
          toast('请选择性别')
          return false
        }
        break
      case 'educ':
        if (condition.value.length > 0) { return true } else {
          toast('请选择最高学历')
          return false
        }
        break
      case 'experience':
        if (condition.value.length > 0) { return true } else {
          toast('请选择工作经验')
          return false
        }
        break
      case 'address':
        if (condition.value.length > 0) { return true } else {
          toast('请选择所在地区')
          return false
        }
        break
      case 'describe':
        if (condition.value.length > 0) { return true } else {
          toast('请填写描述')
          return false
        }
        break
    }
  }
  if (condition) {
    return true
  }
  if (!condition) {
    toast(hint)
    return false
  }
}
/**
 * 登录
 */
function login() {
  return new Promise(function(reject) {
    wx.login({
      success: function(res) {
        reject(res.code)
      }
    })
  })
}
/**
 * 跳转控制
 */
function navigator(url) {
  const pages = getCurrentPages().length
  if (pages >= 10) { return false }
  wx.navigateTo({
    url: url
  })
}
/**
 * 计算年龄
 */
function getAge(birthday) {
  const year = Number(birthday.split('-')[0]),
    mounth = Number(birthday.split('-')[1]),
    day = Number(birthday.split('-')[2]),
    currentMounth = new Date().getMonth() + 1,
    currentDay = new Date().getDate()
  let age = new Date().getFullYear() - year
  if (currentMounth < mounth && currentDay < day) {
    age -= 1
  }
  return age + '岁'
}
/**
 * 选择图片
 */
function chooseImg(count) {
  return new Promise((reslove, reject) => {
    wx.chooseImage({
      sizeType: ['compressed '],
      count: count,
      success: function(res) {
        reslove(res)
      },
      fail: function(res) {
        reject(res)
      }
    })
  })
}
/**
 * 上传图片
 */
function uploadImg(type, src) {
  return new Promise(async(reslove, reject) => {
    console.log(src)
    const token = await ajax(`/qiniu?style=${type}`)
    qiniuUploader.upload(src,
      async(res) => {
        reslove(res)
      }, (error) => {
        wx.showModal({
          title: '提示',
          content: error,
          showCancel: false
        })
        reject(error)
      }, {
        region: 'ECN',
        domain: 'https://pbh6eyvmr.bkt.clouddn.com',
        uptoken: token.data.token
      },
      (res) => {
        wx.showLoading({
          title: '上传中' + res.progress + '%',
          mask: true
        })
      })
  })
}
/**
 * 保存成功
 */
function saveSuccess(text, num = 1) {
  const t = text || '保存成功！'
  toast(t)
  setTimeout(() => {
    wx.navigateBack({ delta: num })
  }, 1000)
}
/**
 * sleep
 */
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}
/**
 * 获取当前时间
 */
function getDate() {
  const date = new Date()
  return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
}
/**
 * 弹窗
 */
function alert(msg) {
  wx.showModal({
    title: '提示',
    content: msg,
    showCancel: false,
    confirmText: '确定',
    confirmColor: '#3CC51F'
  })
}
//  设置tabbr第一项
function setTabBar(val) {
  if (Number(val) === 1) {
    wx.setTabBarItem({
      index: 0,
      iconPath: 'static/img/ic_chance_nor.png',
      selectedIconPath: 'static/img/ic_chance_selected.png',
      pagePath: 'pages/tab-bar/index',
      text: '机会'
    })
  } else if (Number(val) === 2) {
    wx.setTabBarItem({
      index: 0,
      iconPath: 'static/img/ic_people_nor.png',
      selectedIconPath: 'static/img/ic_people_selected.png',
      pagePath: 'pages/tab-bar/index',
      text: '人才'
    })
  }
}
//  深拷贝
function deepClone(obj) {
  let _obj = JSON.stringify(obj),
    objClone = JSON.parse(_obj)
  return objClone
}
export default {
  ajax,
  validate,
  toast,
  login,
  navigator,
  getAge,
  chooseImg,
  uploadImg,
  saveSuccess,
  sleep,
  getDate,
  alert,
  setTabBar,
  deepClone
}
