module.exports = [{
    path: 'pages/tab-bar/index',
    config: {
      enablePullDownRefresh: true
    }
  },
  {
    path: 'pages/tab-bar/my',
    config: {
      navigationBarTitleText: '个人中心'

    }
  },
  {
    path: 'pages/job-seek/job-desc',
    config: {
      navigationBarTitleText: '职位详情'
    }
  },
  {
    path: 'pages/login/login',
    config: {
      navigationBarTitleText: '登录'
    }
  },
  {
    path: 'pages/login/complete-info',
    config: {
      navigationBarTitleText: '完善信息'
    }
  },
  {
    path: 'pages/public/cropper',
    config: {
      navigationBarTitleText: '编辑头像'
    }
  },
  {
    path: 'pages/public/search',
    config: {
      navigationBarTitleText: '搜索'
    }
  },
  {
    path: 'pages/public/reset',
    config: {
      navigationBarTitleText: '重置密码'
    }
  },
  {
    path: 'pages/public/setting',
    config: {
      navigationBarTitleText: '设置'
    }
  },
  {
    path: 'pages/public/textarea',
    config: {
      navigationBarTitleText: '自我描述'
    }
  },
  {
    path: 'pages/recruit/edit-job/edit-address',
    config: {
      navigationBarTitleText: '地址选择'
    }
  },
  {
    path: 'pages/recruit/edit-job/edit-welfare',
    config: {
      navigationBarTitleText: '福利待遇'
    }
  },
  {
    path: 'pages/recruit/edit-school/edit-baseInfo',
    config: {
      navigationBarTitleText: '基本信息'
    }
  },
  {
    path: 'pages/recruit/edit-school/edit-boss',
    config: {
      navigationBarTitleText: '园长信息'
    }
  },
  {
    path: 'pages/recruit/edit-school/edit-desc',
    config: {
      navigationBarTitleText: '园所简介'
    }
  },
  {
    path: 'pages/recruit/edit-school/edit-pic',
    config: {
      navigationBarTitleText: '园所风采'
    }
  },
  {
    path: 'pages/recruit/interview',
    config: {
      navigationBarTitleText: '面试邀请'
    }
  },
  {
    path: 'pages/recruit/job-manage',
    config: {
      navigationBarTitleText: '职位管理'
    }
  },
  {
    path: 'pages/recruit/post-job',
    config: {
      navigationBarTitleText: '发布职位'
    }
  },
  {
    path: 'pages/recruit/rec-manage',
    config: {
      navigationBarTitleText: '简历管理'
    }
  },
  {
    path: 'pages/recruit/resume-collection',
    config: {
      navigationBarTitleText: '简历收藏'
    }
  },
  {
    path: 'pages/recruit/resume-desc',
    config: {
      navigationBarTitleText: '简历详情'
    }
  },
  {
    path: 'pages/recruit/resume-status',
    config: {
      navigationBarTitleText: '简历状态'
    }
  },
  {
    path: 'pages/recruit/school-desc',
    config: {
      navigationBarTitleText: '园所详情'
    }
  },
  {
    path: 'pages/job-seek/job-subscription',
    config: {
      navigationBarTitleText: '职位订阅'
    }
  },
  {
    path: 'pages/job-seek/my-resume',
    config: {
      navigationBarTitleText: '我的简历'
    }
  },
  {
    path: 'pages/job-seek/my-send',
    config: {
      navigationBarTitleText: '我的投递'
    }
  },
  {
    path: 'pages/job-seek/school-desc',
    config: {
      navigationBarTitleText: '园所详情'
    }
  },
  {
    path: 'pages/job-seek/job-collection',
    config: {
      navigationBarTitleText: '职位收藏'
    }
  },
  {
    path: 'pages/job-seek/send-status',
    config: {
      navigationBarTitleText: '投递详情'
    }
  },
  {
    path: 'pages/job-seek/edit-resume/edit-educ',
    config: {
      navigationBarTitleText: '教育经历'
    }
  },
  {
    path: 'pages/job-seek/edit-resume/edit-exp',
    config: {
      navigationBarTitleText: '工作经验'
    }
  },
  {
    path: 'pages/job-seek/edit-resume/edit-intention',
    config: {
      navigationBarTitleText: '求职意向'
    }
  },
  {
    path: 'pages/job-seek/edit-resume/edit-self',
    config: {
      navigationBarTitleText: '自我描述'
    }
  },
  {
    path: 'pages/job-seek/edit-resume/search-school',
    config: {
      navigationBarTitleText: '园所查找'
    }
  },
  {
    path: 'pages/login/bind-phone',
    config: {
      navigationBarTitleText: '绑定手机号'
    }
  },
  {
    path: 'pages/public/region',
    config: {
      navigationBarTitleText: '地址选择'
    }
  }
]